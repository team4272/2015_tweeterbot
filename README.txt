This is the software that makes the robot tweet.

I now wish I'd left the router in the pit with you guys.  Oh well.

# Intro to the software

The software is installed (the folder is copied to) on the robot at
`/home/lvuser/tweeterbot`.

There are executable programs in the `bin` folder.  Their names should
be self-explanatory.

Unfortunately, I never got around to automatic tweets.  It's OK, I
guess because connecting the robot to PAL without the extra router
wouldn't really work.

# Connecting the robot to the internet

Find a wireless router or wireless bridge.  Get it to connect to WiFi,
and share it wired.  This is how the robot connects to the field; but
you should use a different bridge, because messing with the one on the
robot would (obviously) make it not connect to the field.

If you can have the bridge give out IPs (use NAT), have it give them
out in the 10.42.72.X range.  If not, that's fine, but the camera
won't work while not on the field.

I can't give terribly detailed steps on this, because it varies from
device to device.

# Logging into the robot

Connect a computer to the robot.  SSH into it.  On Windows, that means
using PuTTY.  On Mac OS X, open Terminal.app, and run `ssh USER@HOST`.

To log into the robot for most purposes, use:

  User: lvuser
  Host: roboRIO-4272.local

Sometimes, you may need to use the user "admin".

There is no password on either.  If it asks you for one, just press
<enter>.

# Tweeting

The easiest way to tweet is to SSH into the robot, and run the command

    /home/lvuser/tweeterbot/bin/tweet "This is the tweet #omgimarobot"

When Hunter takes a picture, it is stored at
`/home/lvuser/tweeterbot/var/`

You can tweet these pictures by running

    /home/lvuser/tweeterbot/bin/tweetImages "This is the tweet text #omgimarobot" /home/lvuser/tweeterbot/var/FILENAME

# Getting statistics

A spreadsheet with all of the statistics is at
`/usr/local/frc/share/lukeshu.log`.  It grows, you may reset it by
running the command `: > /usr/local/frc/share/lukeshu.log`.

If you rename it from `.log` to `.csv` it should open in Excel/any
spreadsheet program.

It includes a header saying what each column is when the robot starts
up, this is how you can separate matches from eachother.

Columns starting with "ds:" are information from the driver station.
Alliance, match time, battery voltage (why is that coming from the DS
instead of the robot itself? IDK, that's the way it is)

Columns starting with "c:" are things under control of the
driver/autonomous program.  They represent what we are telling the
robot to do at that moment.

Columns starting with "i:" are inputs from the robot; sensors.

Distances are all in "encoder clicks", because we never calibrated the
encoders.

# Trouble-shooting

If it errors when you tweet, make sure that the time/date on the robot
is correct, by running the `date` command.

If the date/time is wrong, log in as "admin" (not lvuser), and run the command:

    date --set="THE CORRECT DATE"

It's kinda flexible on the date format, but I forget the exact rules.
You'll figure it out.
